<?php 


$user = $_SESSION['users'];

if($user == null){
    redirect(site_url(['user', 'login']));
}

$newsNoticas = '';
foreach($user as $row){
    $idUser = $row['id'];
}
$newss = $this->Noticias_model->getAllNewsUsu($idUser);

if($newss == null){
    redirect(site_url(['user', 'crudFuente']));
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url() ?>css/imgEs.css">
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <img class="ads" style="max-width:200px; margin-top: -10px;" src="<?= base_url() ?>img/logo.jpg">


        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            <form action="<?php echo site_url('user/cargarNoticias');?>" class="form-inline my-2 my-lg-0">
            <button type="submit" name="submit" class="btn ">Refrescar Noticias</button> 
            </form>
                <li class="nav-item dropdown">
                </li>
            </ul>
            <form action="<?php echo site_url('user/cargarNoticias');?>" class="form-inline my-2 my-lg-0">
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="margin-right: 90px;">
                        <?php 
                        foreach($user as $row){
                            echo($row['first_name']);
                            $idUser = $row['id'];
                        }
                        ?>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="<?php echo site_url('user/login');?>">Logout</a>
                        <a class="dropdown-item" href="<?php echo site_url('user/crudFuente');?>">News Sources</a>
                        <?php if($row["role_id"] == 1){?>
                            
                        <a class="dropdown-item" href="<?php echo site_url('user/crudCategoria');?>">Categories</a>
                        <?php
                        }if($row["role_id"] == 2){

                        }
                        ?>
                    </div>
                </div>
            </form>
        </div>
    </nav>

    <div class="container pt-3">
        <h1 class="text-center">Your unique News Cover</h1>
        <div class="container">
            <hr>

        </div>



        <div class="card-deck">
        <form method="post" action="" >
        <input type="search" name="idC" placeholder="Search" value="">
        </form>
            <?php
                $categories = $this->Fuente_model->getCategoria();
                $data['categories'] = $categories;
                foreach ($categories as $rows){
                    ?>
            <div class="card" style="width: 16rem;">
                <ul class="list-group list-group-flush">
                    <form method="post" action="" >
                    <input style="visibility:hidden;" type="text" name="idC" placeholder="Enter website feed URL" value="<?php echo ($rows['id']);?>">
                    <button type="submit" name="submit" class="btn "><?php echo ($rows['names']);?></button> 
                    </form>
                    
                </ul>
            </div>
            <?php
                }
                ?>
        </div>
        <br><br>


        <?php


            if(isset($_POST['submit'])){
                if($_POST['idC'] != ''){
                $url = $_POST['idC'];
                
                $news = $this->Noticias_model->getAllNoticias($url, $idUser);
                
                //$cons = "SELECT * FROM news WHERE category_id = '$url' AND user_id = '$userI'";
               // $query = mysqli_query($conn, $cons);
                //$array = mysqli_fetch_array($query);

                }if ($_POST['idC'] == null){

                }
            }



            ?>

        <div class="card-columns">
          <?php
            if($news != null){
                
                foreach ($news as $row){
                    if($row['user_id'] == $idUser){
                    ?>
            <div class="card mb-3">
                <div class="card-body">
                    <p class="card-text"><?php echo ($row['date']);?></p>
                </div>
                <!--<img class="card-img-top" src="" alt="Card image cap">-->
                <div class="card-body">
                    <h5 class="card-title"><?php echo ($row['title']);?></h5>
                    <h6 class="card-title"><?php 
                    foreach ($categories as $rowss){
                        if($row['category_id'] == $rowss['id'])
                    echo ($rowss['names']);
                    }
                    ?></h6>
                    <p class="card-text"><?php echo ($row['short_description']);?></p>
                </div>
                <div class="card-footer">
                    <a href="<?php echo ($row['permanlink']);?>" class="card-link">Ver Noticia</a>
                </div>
            </div>
            <?php
                    }
                }
            }if($newsNoticas == null){
                
            }
                ?>
        </div>

        <footer>
            <!-- <hr id="hr3">
        <div class="contenedor-footer">
            <div class="content-foo">
                <h4>Incio</h4>
            </div>
            <div class="content-fo">
                <h4>|</h4>
            </div>
            <div class="content-foo">
                <h4>Cambalache</h4>
            </div>
            <div class="content-fo">
                <h4>|</h4>
            </div>
            <div class="content-foo">
                <h4>Ingresar</h4>
            </div>
        </div>
        <h2 class="titulo-final">&copy; Cambalache.net</h2>-->

            <nav>
                <hr>
                <div class="container text-center">
                    <div class="content-fooa">
                        <h4> <a href="">My Cover</a> | <a href="">About</a> | <a href="">Help</a></h4>
                    </div>
                    <p class="navbar-text col-md-12 col-sm-12 col-xs-12">&copy; My News Cover</p>
                </div>
            </nav>
        </footer>

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
        </script>
</body>

</html>