<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *    - or -
     *         http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function login()
    {
        session_destroy();
        $this->load->view('users/index');
    }

    public function registro()
    {
        $this->load->view('users/registro');
    }

    public function dashboard()
    {
        $this->load->view('users/dashboard');
    }

    public function crudFuente()
    {
        redirect(site_url(['user', 'FuenteLista']));
    }

    public function addFuente()
    {
        redirect(site_url(['user', 'categoriaSelect']));
    }

    public function crudCategoria()
    {
        redirect(site_url(['user', 'categoriaLista']));
    }

    public function addCategoria()
    {
        $this->load->view('users/addCategories');
    }

    public function editFuentes()
    {
        $this->load->view('users/editnews');
    }

    public function editCategorias()
    {
        $this->load->view('users/editCateg');
    }

    public function authenticate()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $user = $this->User_model->authenticate($username, $password);

        if ($user) {
            session_start();
            $_SESSION['users'] = $user;
            redirect('user/NewsLista');
        } else {
            redirect('user/login');
        }

    }

    public function registrar()
    {
        $email = $this->input->post('email');
        $contra = $this->input->post('Passwords');
        $result = $this->User_model->insert($this->input->post());

        if ($result) {
            $this->session->set_flashdata('msg', 'User created, please login');
            $to = $email;
            $subject = "Verificación de correo electronico";
            $message = "La contraseña es: $contra";
 
            mail($to, $subject, $message);
            redirect(site_url(['user', 'login']));
        } else {
            // send errors
            redirect(site_url(['user', 'registro']));
        }

    }

    public function eliminarCategoria($id)
    {
        $categories = $this->Categoria_model->delete($id);
        redirect(site_url(['user', 'categoriaLista']));
    }

    public function editarCateg($id)
    {
        $categories = $this->Categoria_model->editC($id);
        $data['categories'] = $categories;

        $this->load->view('users/editCateg', $data);
    }

    public function editarCategoria()
    {
        $id = $this->input->post('id');
        $names = $this->input->post('names');

        $result = $this->Categoria_model->update($id, $names);
        if ($result) {
            redirect(site_url(['user', 'categoriaLista']));
        } else {
            // send errors
            redirect(site_url(['user', 'addCategoria']));
        }
    }

    public function categoriaLista()
    {
        $categories = $this->Categoria_model->getAll();
        $data['categories'] = $categories;

        $this->load->view('users/crudCategories', $data);
    }

    public function categoriaSelect()
    {
        $categories = $this->Categoria_model->getAll();
        $data['categories'] = $categories;

        $this->load->view('users/addnews', $data);
    }

    public function registrarCategoria()
    {

        $result = $this->Categoria_model->insert($this->input->post());

        if ($result) {
            redirect(site_url(['user', 'categoriaLista']));
        } else {
            // send errors
            redirect(site_url(['user', 'addCategoria']));
        }

    }

    public function FuenteLista()
    {
        $user = $_SESSION['users'];
        foreach ($user as $row) {
            $user_id = $row['id'];
        }
        $sources = $this->Fuente_model->getAll($user_id);
        $data['newssources'] = $sources;

        $this->load->view('users/crud', $data);
    }

    public function registrarFuente()
    {
        $url = $this->input->post('urls');
        $name = $this->input->post('nameN');
        $category_id = $this->input->post('category_id');

        $user = $_SESSION['users'];
        foreach ($user as $row) {
            $user_id = $row['id'];
        }

        $result = $this->Fuente_model->insert($url, $name, $category_id, $user_id);

        if ($result) {
            redirect(site_url(['user', 'FuenteLista']));
        } else {
            // send errors
            redirect(site_url(['user', 'addCategoria']));
        }

    }

    public function eliminarFuente($id)
    {
        $newssources = $this->Fuente_model->delete($id);
        redirect(site_url(['user', 'FuenteLista']));
    }

    public function categoriaSelectE()
    {
        $categories = $this->Categoria_model->getAll();
        $data['categories'] = $categories;

        $this->load->view('users/editnews', $data);
    }

    public function editarFuent($id)
    {
        $newssources = $this->Fuente_model->editF($id);
        $data['newssources'] = $newssources;
        //print_r($data);

        $this->load->view('users/editnews', $data);
    }

    public function editarFuente($id)
    {
        $nombre = $this->input->post('name');
        $url = $this->input->post('url');
        $category_id = $this->input->post('category');

        $result = $this->Fuente_model->update($id, $url, $nombre, $category_id);
        if ($result) {
            redirect(site_url(['user', 'FuenteLista']));
        } else {
            // send errors
            redirect(site_url(['user', 'addCategoria']));
        }
    }

    public function NewsLista()
    {
        $news = $this->Noticias_model->getAllNews();
        $data['news'] = $news;

        $this->load->view('users/dashboard', $data);
    }

    public function cargarNoticias()
    {
        $newssources = $this->Fuente_model->getFuenteNoticia();
        $data['newssources'] = $newssources;

        $news = $this->Noticias_model->deleteNews();

        foreach ($newssources as $rowss) {
            $url = $rowss['urls'];
            $category = $rowss['category_id'];
            $user_id = $rowss['user_id'];
            $id_newssource = $rowss['id'];

            $invalidurl = false;
            if (@simplexml_load_file($url)) {
                $feeds = simplexml_load_file($url);
            } else {
                $invalidurl = true;
            }

            $i = 0;
            if (!empty($feeds)) {

                $site = $feeds->channel->title;
                $sitelink = $feeds->channel->link;

                foreach ($feeds->channel->item as $item) {

                    $title = $item->title;
                    $link = $item->link;
                    $description = $item->description;
                    $postDate = $item->pubDate;
                    $pubDate = date('D, d M Y', strtotime($postDate));

                    //$insertar = "INSERT INTO news VALUES ('','$title','$description','$link', '$pubDate', $id_newssource, $user_id, $category) ";
                    $result = $this->Noticias_model->insertNews($title,$description,$link, $pubDate, $id_newssource, $user_id, $category);

                    if ($i >= 16) {
                        
                        break;
                        
                    }
                    $i++;
                }
            }

        }
        redirect(site_url(['user', 'NewsLista']));
    }

}
