<?php

class Fuente_model extends CI_Model
{

    /**
     *  Inserts a new user in the database
     *
     * @param $user  An associative array with all user data
     */
    public function insert($url, $name, $category_id, $user_id)
    {
        $data = array(

            'urls'=>$url,
            'nameN'=>$name,
            'category_id'=>$category_id,
            'user_id'=>$user_id,
    
             );

        $query = $this->db->insert('newssources', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getAll($id)
    {
        $this->db->select('a.*,d.names');
        $this->db->from('newssources a');
        $this->db->join('categories d', 'a.category_id = d.id');
        $this->db->where('a.user_id', $id);

        $aResult = $this->db->get();

        if (!$aResult->num_rows() == 1) {
            return false;
        }

        return $aResult->result_array();
    }

    public function getCategoria()
    {
        $query = $this->db->get('categories');
        return $query->result_array();
    }

    public function getFuenteNoticia()
    {
        $query = $this->db->get('newssources');
        return $query->result_array();
    }


    public function editF($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('newssources');

        return $query->result_array();

    }

    public function delete($id)
    {
        $this->db->delete('newssources', array('id' => $id));
    }

    public function update($id, $url, $name, $category_id)
    {
        $this->db->where('id', $id);
        $this->db->set('urls', $url);
        $this->db->set('nameN', $name);
        $this->db->set('category_id', $category_id);
        return $this->db->update('newssources');
    }

}
