<?php

class Categoria_model extends CI_Model
{

    /**
     *  Inserts a new user in the database
     *
     * @param $user  An associative array with all user data
     */
    public function insert($categoria)
    {
        $query = $this->db->insert('categories', $categoria);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getAll()
    {
        $query = $this->db->get('categories');
        return $query->result_array();
    }

    public function editC($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('categories');
        
        return $query->result_array();
    
      }

    public function delete($id)
    {
        $this->db->delete('categories', array('id' => $id));
    }

    public function update($id, $nombre)
    {
        $this->db->where('id', $id);
        $this->db->set('names', $nombre);
        return $this->db->update('categories');
    }

}
