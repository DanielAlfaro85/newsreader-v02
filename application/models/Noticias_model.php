<?php

class Noticias_model extends CI_Model
{

    /**
     *  Inserts a new user in the database
     *
     * @param $user  An associative array with all user data
     */
    public function insert($url, $name, $category_id, $user_id)
    {
        $data = array(

            'urls' => $url,
            'nameN' => $name,
            'category_id' => $category_id,
            'user_id' => $user_id,

        );

        $query = $this->db->insert('newssources', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getAll()
    {
        $this->db->select('a.*,d.names');
        $this->db->from('newssources a');
        $this->db->join('categories d', 'a.category_id = d.id');

        $aResult = $this->db->get();

        if (!$aResult->num_rows() == 1) {
            return false;
        }

        return $aResult->result_array();
    }

    public function getAllNews()
    {
        $query = $this->db->get('news');
        return $query->result_array();
    }
    
    public function getAllNewsUsu($id)
    {
        $this->db->where('user_id', $id);
        $query = $this->db->get('news');
        return $query->result_array();
    }

    public function getAllNoticias($url, $idUser)
    {
        $this->db->where('category_id', $url);
        $this->db->where('user_id', $idUser);
        $query = $this->db->get('news');
        return $query->result_array();
    }

    public function getCategoria()
    {
        $query = $this->db->get('categories');
        return $query->result_array();
    }

    public function editF($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('newssources');

        return $query->result_array();

    }

    public function delete($id)
    {
        $this->db->delete('newssources', array('id' => $id));
    }

    public function update($id, $url, $name, $category_id)
    {
        $this->db->where('id', $id);
        $this->db->set('urls', $url);
        $this->db->set('nameN', $name);
        $this->db->set('category_id', $category_id);
        return $this->db->update('newssources');
    }

    public function deleteNews()
    {
        $this->db->empty_table('news');
    }

    public function insertNews($title, $description, $link, $pubDate, $id_newssource, $user_id, $category)
    {
        $data = array(

            'title' => $title,
            'short_description' => $description,
            'permanlink' => $link,
            'Date' => $pubDate,
            'news_source_id' => $id_newssource,
            'user_id' => $user_id,
            'category_id' => $category,

        );

        $query = $this->db->insert('news', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

}
